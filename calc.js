function CalcController($scope){
	$scope.sum=0;
	$scope.payment_time=0;
	$scope.percentage=0;
	$scope.payment_frequency=0;
	$scope.sum1=0;
	$scope.sum2=0;
	$scope.datefrom="";
	$scope.dateto="";
	$scope.percentage1=0;
	$scope.percentage2=0;
	$scope.period=0;

	$scope.freq_list=[
	{name:'месяц', value:1},
	{name:'квартал', value:3},
	{name:'полугодие', value:6},
	{name:'год', value:12},
	{name:'произвольный', value:0}
	];

	$scope.payment_count=0;

	function parseDate(num){
		var parts=num.split('-');
		return new Date(parts[0], parts[1]-1, parts[2]);
	}

	$scope.SetSum=function(num){
		$scope.sum=num;
	}

	$scope.SetSum1=function(num){
		$scope.sum1=num;
	}

	$scope.SetSum2=function(num){
		$scope.sum2=num;
	}

	$scope.SetPaymentTime=function(num){
		$scope.payment_time=num;
	}

	$scope.SetPercentage1=function(num){		
		$scope.percentage1=num;
	}

	$scope.SetPercentage2=function(num){		
		$scope.percentage2=num;
	}

	$scope.SetPaymentFrequency=function(num){
		$scope.payment_frequency=num;
	}

	$scope.SetDateFrom=function(num){
		$scope.datefrom=num;
		console.log(parseDate(num));
		console.log(Date.parse(num));

	}
	$scope.SetDateTo=function(num){
		$scope.dateto=num;
		console.log(parseDate(num));
		console.log(Date.parse(num));
	}

	$scope.GetPeriod=function(){
		if ($scope.datefrom != 0 & $scope.dateto != 0 & Date.parse($scope.dateto)>Date.parse($scope.datefrom)){
			$scope.period=(Date.parse($scope.dateto)-Date.parse($scope.datefrom))/8.64e7/30 | 0;
			$scope.payment_time=$scope.period;
			return $scope.period;
		}
		else{
			return 0;
		}
	}
	$scope.SetPaymentCount=function(num){
		$scope.payment_count=num;
		$scope.payment_frequency=Math.round($scope.period/$scope.payment_count);
		console.log($scope.payment_count);
	}
	$scope.GetPaymentCount=function(){
		if ($scope.payment_frequency==0 || $scope.payment_time==0)
		{
			$scope.payment_count=0;
		}
		else
		{
			$scope.payment_count=Math.round($scope.payment_time/$scope.payment_frequency);
		}
		return $scope.payment_count;
	}

	//$scope.payment_per_month=0;

	$scope.GetPaymentPerMonth=function(){
		if ($scope.sum!=0 || $scope.payment_count!=0)
		{
			$scope.payment_per_month=Math.round($scope.sum/$scope.payment_count,2);
		}
		return $scope.payment_per_month;
	}

	$scope.graph=[];

	$scope.BuildGraph=function(){
		var list=[];
		var main_dept=Math.round($scope.sum1/$scope.payment_count);
		var percent1=$scope.sum1*$scope.percentage1/100;
		var percent2=$scope.sum1*$scope.percentage2/100;
		var sum1=parseInt($scope.sum1)+parseInt(percent1);
		var sum2=parseInt($scope.sum2)+parseInt(percent2);
		parts=$scope.datefrom.split('-');
		for (var i=0; i<$scope.payment_count;i++){
			reward1=(sum1*$scope.percentage1/100)/($scope.payment_count);
			reward2=(sum1*$scope.percentage2/100)/($scope.payment_count);
			list.push({payment:i+1, date:parts[2]+'-'+parseInt(parts[1])+'-'+parts[0], main_dept:Math.round(main_dept), total:Math.round(reward1+reward2), reward1:Math.round(reward1), reward2:Math.round(reward2), left:0});
			sum1=sum1-reward1-reward2-main_dept;
			if (parts[1]+$scope.payment_frequency>12){
				month=parseInt(parts[1])+$scope.payment_frequency;
				console.log(month);

				parts[0]=parseInt(parts[0])+Math.floor(month/12);
				parts[1]=month % 12;
			}
			else{
				parts[1]=(parseInt(parts[1])+$scope.payment_frequency);
			}
			
		}
		$scope.graph=list;
	}
}