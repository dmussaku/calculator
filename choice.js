function DAO_Controller($scope){
	$scope.dao_list=[
	{name:'Фонд финансовой поддержки сельского хозяйства',choice:'0'},
	{name:'Аграрная кредитная корпорация',choice:'1'},
	{name:'КазАгроФинанс',choice:'2'}
	];

	var purpose_list=[
		{dao_choice:'0',name:'Организация или расширение собственного дела',choice:'0'},
		{dao_choice:'0',name:'Проведение весенне – полевых и уборочных работ',choice:'1'},
		{dao_choice:'0',name:'Развитие селькой инфроструктиры',choice:'2'},
		{dao_choice:'0',name:'Лизинг комплекса мини услуг',choice:'3'},
		{dao_choice:'0',name:'Пополнение основных стредств',choice:'4'},
		{dao_choice:'0',name:'Развитие животноводства и растениеводства',choice:'5'},
		{dao_choice:'0',name:'Кредитование МКО для последующего кредитования населения',choice:'6'},

		{dao_choice:'1',name:'Развитие сельской инфроструктуры',choice:'7'},
		{dao_choice:'1',name:'Формирование агрорной инфроструктуры для производства',choice:'8'},
		{dao_choice:'1',name:'Кредитование КТо для последующегоо финансирования участников КТ',choice:'9'},
		{dao_choice:'1',name:'Приобритение КРС',choice:'10'},
		{dao_choice:'1',name:'Обновление и модернезация устаревшего оборудования',choice:'11'},
		{dao_choice:'1',name:'Проведение весенне полевых и уборочных работ',choice:'12'},
		{dao_choice:'1',name:'Развитие животноводства и растениеводства',choice:'13'},
		{dao_choice:'1',name:'Финансирование основных видов деятельности',choice:'14'},

		{dao_choice:'2',name:'Приобритение КРС',choice:'15'},
		{dao_choice:'2',name:'Лизинг сельхозтехники',choice:'16'},
		{dao_choice:'2',name:'Пополнение основных стредств',choice:'17'}
		
		];

	var program_list=[
		{purpose_choice:"0", name:"Занятость 2020", choice:'0'},
		{purpose_choice:"1", name:"Егинжай", choice:'1'},
		{purpose_choice:"2", name:"Несельскохозяйственные виды бизнеса на селе", choice:'3'},
		{purpose_choice:"3", name:"Лизинг комплекса мини услуг", choice:'4'},
		{purpose_choice:"4", name:"Мурахаба", choice:'5'},
		{purpose_choice:"5", name:"Бизнес регионы", choice:'6'},

		{purpose_choice:"6", name:"Кредитование МКО на развитие животноводства и растениеводства и других видов бизнеса на селе", choice:'7'},
		{purpose_choice:"6", name:"Кредитование МКО на закуп КРС", choice:'8'},
		{purpose_choice:"6", name:"Кредитование МКО на проведение ВП и УР", choice:'9'},

		{purpose_choice:"7", name:"Несельскохозяйственные виды бизнеса на селе (НСХБ)", choice:'10'},
		{purpose_choice:"8", name:"Кредитование обьяденений СХТБ", choice:'11'},

		{purpose_choice:"9", name:"Кредитование сельскохозяйственого производства и промышленности, перерабатывающей сельскохозяйственную продукцию", choice:'12'},
		{purpose_choice:"9", name:"Кредитование КТ, для последующего финансирования участников КТ", choice:'13'},

		{purpose_choice:"10", name:"Сыбага", choice:'14'},
		{purpose_choice:"11", name:"Кредитование предприятиии переработки сельскохозяйственного сырья и производство продуктов питания", choice:'9'},
		{purpose_choice:"19", name:"Поддержка субъектов АПК", choice:'15'},
		{purpose_choice:"13", name:"Коммерческое кредитование проектов развития АПК", choice:'16'},
		{purpose_choice:"14", name:"Кредитование специализированных оргоназицаций", choice:'17'},
		{purpose_choice:"15", name:"Племенные животные", choice:'18'},

		{purpose_choice:"16", name:"Зерносушилки", choice:'19'},
		{purpose_choice:"16", name:"Сельхозтехника не требующая монтажа", choice:'20'},
		{purpose_choice:"16", name:"Технологическое оборудование и специальная техника требующая монтажа", choice:'21'},
		{purpose_choice:"16", name:"Перерабатывающее оборудование", choice:'22'},
		{purpose_choice:"16", name:"Лизинг техники бывшей в эксплуатации", choice:'23'},
		{purpose_choice:"16", name:"Мелиотеративная техника", choice:'24'},
		{purpose_choice:"16", name:"Траснпортные стредства для перевозки сельскохозяйственной продукции, навесных, прицепных машин", choice:'25'},
		{purpose_choice:"16", name:"Сельскохозяйственные самолеты", choice:'26'},
		{purpose_choice:"16", name:"Возобновляемые источники энергии ", choice:'27'},
		{purpose_choice:"16", name:"Экспресс лизинг (сельхозтехника, специальная техника, не требующего монтажа)", choice:'28'},
		
		{purpose_choice:"17", name:"Кредитование оборотного капитала", choice:'29'},
		{purpose_choice:"17", name:"Кредитование основных средств", choice:'30'},



	];
	$scope.dao_choice=0;
	$scope.purpose_choce=0;

	$scope.GetPurposeList = function(s){
		var list=[];
		
		for (var purpose1 in purpose_list)
		{
			if (purpose_list[purpose1].dao_choice==s)
			{
				list.push(purpose_list[purpose1]);	
			}
		}
		return list;
	};

	$scope.SetDaoChoice = function(dao_choice){
		$scope.dao_choice=dao_choice;
	};

	$scope.GetProgramList = function(purpose_choice){
		var list=[];
		
		for (var program in program_list)
		{
			if (program_list[program].purpose_choice==purpose_choice)
			{
				list.push(program_list[program]);	
			}
		}
		return list;
	};

	$scope.SetPurposeChoice=function(purpose_choice){
		$scope.purpose_choice=purpose_choice;
	};
}
